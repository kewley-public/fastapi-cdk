from aws_cdk import (
    Stack,
    aws_apigateway as api,
)
from constructs import Construct

from infrastructure.lambdas import get_app_function
from infrastructure.util import get_common_lambda_props, Lambda


class FastapiCdkStack(Stack):
    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        env_name = kwargs["env"].name

        # ----------------------------------------
        # FASTAPI APP
        # ----------------------------------------
        flask_props = get_common_lambda_props(self, env_name, Lambda.FAST_API_APP)
        fast_api = get_app_function(self, Lambda.FAST_API_APP, shared_props=flask_props)

        # ----------------------------------------
        # API GW
        # ----------------------------------------
        rest_api = api.RestApi(
            self, "fastapi-api", rest_api_name=f"fastapi-api-{env_name}"
        )

        root_api = rest_api.root.add_resource("api")
        v1_api = root_api.add_resource("v1")

        api.ProxyResource(
            self,
            f"FastAPIProxyResource{env_name.title()}",
            parent=v1_api,
            any_method=True,
            default_integration=api.LambdaIntegration(fast_api, proxy=True),
        )
