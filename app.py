import os

from aws_cdk import App, Environment
from dotenv import load_dotenv
from infrastructure.fastapi_cdk_stack import FastapiCdkStack
from infrastructure.util import check_required_env_vars


class ExtendedEnvironment(Environment):
    def __init__(self, account: str, name: str, region: str):
        super().__init__(account=account, region=region)
        self.name = name


VALID_ENVS = {"dev", "prod"}

errors = []

env = os.getenv("ENV", "dev")
if env not in VALID_ENVS:
    raise ValueError(f"ENV must be one of {VALID_ENVS}")

load_dotenv()
load_dotenv(dotenv_path=f".env.{env}")

check_required_env_vars("CDK_DEFAULT_ACCOUNT", "CDK_DEFAULT_REGION")


app = App()
FastapiCdkStack(
    app,
    f"FastAPICdkStack{env.title()}",
    env=ExtendedEnvironment(
        account=os.environ["CDK_DEFAULT_ACCOUNT"],
        name=env,
        region=os.environ["CDK_DEFAULT_REGION"],
    ),
)

app.synth()
