import uuid
from fastapi import FastAPI, APIRouter, HTTPException
from mangum import Mangum
from pydantic import BaseModel, Field
from uuid import UUID


class User(BaseModel):
    id: UUID
    first_name: str = Field(
        default=None, title="The first name of the user", max_length=300
    )
    last_name: str = Field(
        default=None, title="The last name of the user", max_length=300
    )


app = FastAPI(redirect_slashes=False)
router = APIRouter(prefix="/api/v1")

users = [
    User(id=uuid.uuid4(), first_name="Jack", last_name="Sparrow"),
    User(id=uuid.uuid4(), first_name="Mike", last_name="Hawk"),
]


@router.get("/")
def get_root():
    return {"message": "FastAPI running"}


@router.get("/users")
async def get_users() -> list[User]:
    return users


@router.get("/users/{user_id}")
async def read_user(user_id: UUID) -> User:
    for u in users:
        if u.id == user_id:
            return u

    raise HTTPException(status_code=404, detail="User not found")


app.include_router(router)

handler = Mangum(app)

if __name__ == "__main__":
    # For running locally with IntelliJ Debugger
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)
